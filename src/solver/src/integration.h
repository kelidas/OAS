#ifndef _INTEGRATION_H
#define _INTEGRATION_H

#include "node_container.h"
#include "shape_functions.h"

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// INTEGRATION POINTS - MASTER CLASS
class IntegrationType
{
private:

protected:
    std :: string name;
    std :: vector< Point >ip_locs;
    std :: vector< double >ip_weights;
public:
    IntegrationType() { name = "basic integration type"; }
    virtual ~IntegrationType() {};
    unsigned giveNumIP() const { return ip_locs.size(); };
    double giveIPWeight(unsigned i) const { return ip_weights [ i ]; };
    void setIPWeight(unsigned i, double w) { ip_weights [ i ] = w; };
    void setIPLocation(unsigned i, Point p) { ip_locs [ i ] = p; };
    Point giveIPLocation(unsigned i) const { return ip_locs [ i ]; };
    Point *giveIPLocationPointer(unsigned i) { return & ( ip_locs [ i ] ); };
    virtual void init();
    virtual void init(const std :: vector< Node * > &nodes);
    virtual void init(const std :: vector< Node * > &nodes, const std :: vector< std :: vector< unsigned > > &faces, Point *centroid);
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// EMPTY INTEGRATION RULE
class EmptyIntegration : public IntegrationType
{
public:
    EmptyIntegration() { name = "EmptyIntegration"; };
    virtual ~EmptyIntegration() {};
    virtual void init();
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// ONE POINT INTEGRATION FOR DISCRETE MODELS
class IntegrDiscrete1 : public IntegrationType
{
public:
    IntegrDiscrete1() { name = "IntegrDiscrete1"; };
    virtual ~IntegrDiscrete1() {};
    virtual void init();
    void setNumIP(unsigned n);
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// FOUR POINT INTEGRATION IN SQUARE
class IntegrQuad4 : public IntegrationType
{
public:
    IntegrQuad4() { name = "IntegrQuad4"; };
    virtual ~IntegrQuad4() {};
    virtual void init();
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// ONE POINT INTEGRATION IN TRIANGLE
class IntegrTri1 : public IntegrationType
{
public:
    IntegrTri1() { name = "IntegrTri1"; };
    virtual ~IntegrTri1() {};
    virtual void init();
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// THREE POINT INTEGRATION IN TRIANGLE
class IntegrTri3 : public IntegrationType
{
public:
    IntegrTri3() { name = "IntegrTri3"; };
    virtual ~IntegrTri3() {};
    virtual void init();
};


//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// INTEGRATION IN POLYGON
class IntegrPolygon : public IntegrationType
{
    std :: string ip_type;
public:
    IntegrPolygon(std :: string type) { name = "IntegrPolygon"; ip_type = type; };
    virtual ~IntegrPolygon() {};
    virtual void init(const std :: vector< Node * > &nodes, const std :: vector< std :: vector< unsigned > > &faces, Point *centroid);
};


//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// EIGHT POINT INTEGRATION IN CUBE
class IntegrBrick8 : public IntegrationType
{
public:
    IntegrBrick8() { name = "IntegrBrick8"; };
    virtual ~IntegrBrick8() {};
    virtual void init();
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// ONE POINT INTEGRATION IN TETRA
class IntegrTetra4 : public IntegrationType
{
public:
    IntegrTetra4() { name = "IntegrTetra4"; };
    virtual ~IntegrTetra4() {};
    virtual void init();
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// TWELVE POINT INTEGRATION IN LDPM SIMPLEX
class IntegrLDPM12 : public IntegrationType
{
public:
    IntegrLDPM12() { name = "IntegrLLDPM12"; };
    virtual ~IntegrLDPM12() {};
    virtual void init();
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// FIBER INTEGRATION
class IntegrFiber : public IntegrationType
{
public:
    IntegrFiber() { name = "IntegrFiber"; };
    virtual ~IntegrFiber() {};
    virtual void init();
    void addNewIP(Point location);
};



#endif  /* _INTEGRATION_H */
